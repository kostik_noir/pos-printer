(function () {
  /* global chrome:true */

  const WINDOW_ID = 'pos-app';
  const URL = './app/index.html';
  const CREATE_WINDOW_OPTIONS = {
    id: WINDOW_ID,
    innerBounds: {
      width: 400,
      height: 600
    }
  };

  chrome.app.runtime.onLaunched.addListener(() => {
    runApp();
  });
  chrome.app.runtime.onRestarted.addListener(() => {
    runApp();
  });

  function runApp() {
    chrome.app.window.create(URL, CREATE_WINDOW_OPTIONS);
  }
})();
