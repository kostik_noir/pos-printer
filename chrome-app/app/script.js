(function () {
  /* eslint-disable no-console */

  let webView;

  setup();

  function setup() {
    webView = document.querySelector('#webview');
    webView.addEventListener('loadstop', onExternalContentLoaded, false);
    webView.addEventListener('consolemessage', onExternalContentConsole, false);
  }

  function onExternalContentLoaded() {
    webView.removeEventListener('loadstop', onExternalContentLoaded, false);

    window.pos.printer.backend.setup(window, webView.contentWindow);
  }

  function onExternalContentConsole({ message }) {
    console.log(message);
  }
})();
