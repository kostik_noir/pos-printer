(function () {
  /* eslint-disable no-console */
  /* global chrome:true */

  // ------------------------
  // public
  // ------------------------
  if (typeof window.pos === 'undefined') {
    window.pos = {};
  }
  if (typeof window.pos.printer === 'undefined') {
    window.pos.printer = {};
  }

  window.pos.printer.backend = {
    setup
  };

  // ------------------------
  // private
  // ------------------------
  const HANDSHAKE_MSG_TYPE = 'PRINTER_HANDSHAKE';

  function setup(hostWindow, guestWindow) {
    hostWindow.addEventListener('message', onMessage, false);
    guestWindow.postMessage({
      type: HANDSHAKE_MSG_TYPE
    }, '*');
  }

  function onMessage({ data: { host, port, data } }) {
    port = parseInt(port, 10);
    data = str2ab(data);

    const cfg = {
      host,
      port,
      data
    };

    chrome.sockets.tcp.create({}, onSocketCreated.bind(this, cfg));
  }

  function onSocketCreated({ host, port, data }, { socketId }) {
    chrome.sockets.tcp.connect(socketId, host, port, res => {
      if (res < 0) {
        chrome.sockets.tcp.disconnect(socketId);
        return;
      }

      chrome.sockets.tcp.send(socketId, data, ({ resultCode }) => {
        console.log(`resultCode: ${resultCode}`);
        chrome.sockets.tcp.close(socketId, () => {
          console.log('>>> CLOSED');
        });
      });
    });
    console.log(host, port, data);
  }

  function str2ab(str) {
    const buf = new ArrayBuffer(str.length * 2);
    const bufView = new Uint16Array(buf);
    for (let i = 0, strLen = str.length; i < strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    return buf;
  }
})();
