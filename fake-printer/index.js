/* eslint-disable no-var, no-console */

var net = require('net');
var os = require('os');

var cfg = {
  host: os.platform() === 'linux' ? '0.0.0.0' : '127.0.0.1',
  port: 1337
};

var server = net.createServer(socket => {
  var id = `${socket.remoteAddress}:${socket.remotePort}`;
  console.log(`[CONNECTED]: ${id}`);

  socket.write(`Hello ${id}\r\n`);
  socket.pipe(socket);

  socket.on('data', (data) => {
    console.log(`[MSG]: ${data.toString()}`);
  });

  socket.on('end', () => {
    console.log(`[DISCONNECTED]: ${id}`);
  });
});

server.listen(cfg.port, cfg.host);
