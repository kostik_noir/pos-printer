# POS printer (WIP)

1. run static server  
    It simulates the remote HTTP page  
    `npm run static-server`

2. (in other console) run fake printer server  
    now it only listen tcp packets and print them into console  
    `npm run fake-printer`

3. (in 3rd console) start Chrome app  
    It uses `chrome.sockets.tcp` in order to send data to fake printer server  
    `npm run chrome-app`