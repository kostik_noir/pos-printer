module.exports = {
  root: true,
  extends: 'airbnb',
  env: {
    browser: true,
    jasmine: true,
    node: true
  },
  plugins: [
    'react'
  ],
  rules: {
    'comma-dangle': ['error', 'never'],
    'no-param-reassign': 'off',
    'no-use-before-define': ['error', 'nofunc'],
    'func-names': 'off',
    'default-case': 'off',
    'no-shadow': 'off',
    'vars-on-top': 'off',
    'wrap-iife': ['error', 'inside'],

    'react/jsx-uses-react': 2,
    'react/jsx-uses-vars': 2,
    'react/react-in-jsx-scope': 2,
    'react/sort-comp': 0
  }
};
