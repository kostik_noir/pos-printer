(function () {
  /* eslint-disable no-console */

  // ------------------------
  // public api
  // ------------------------
  if (typeof window.pos === 'undefined') {
    window.pos = {};
  }
  window.pos.printer = {
    send
  };

  // ------------------------
  // private
  // ------------------------
  let backendWindow = null;
  let backendOrigin = null;
  let hasBackend = false;

  function send(msg) {
    if (!hasBackend) {
      console.log('printer backend is not available');
      return;
    }
    backendWindow.postMessage(msg, backendOrigin);
  }

  window.addEventListener('message', onMessage, false);
  function onMessage(event) {
    const msg = event.data;
    switch (msg.type) {
      case 'PRINTER_HANDSHAKE':
        backendWindow = event.source;
        backendOrigin = event.origin;
        hasBackend = true;
        break;
    }
  }
})();
