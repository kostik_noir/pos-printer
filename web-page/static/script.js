(function () {
  /* eslint-disable no-console */
  const form = document.querySelector('#form');
  form.addEventListener('submit', onSubmit, false);

  const hostField = form.querySelector('#host');
  hostField.value = '0.0.0.0';

  const portField = form.querySelector('#port');
  portField.value = '1337';

  const msgField = form.querySelector('#msg');
  msgField.value = 'msg';

  function onSubmit(event) {
    event.preventDefault();

    const hostValue = hostField.value;
    const hostValueIsValid = validateHostValue(hostValue);

    if (!hostValueIsValid) {
      console.log('Invalid value of host');
      return;
    }

    const portValue = portField.value;
    const portValueIsValid = validatePortValue(portValue);
    if (!portValueIsValid) {
      console.log('Invalid value of port');
      return;
    }

    const msgValue = msgField.value;
    if (!validateMsgValue(msgValue)) {
      console.log('Message should be not empty');
      return;
    }

    send(hostValue, portValue, msgValue);
  }

  function send(host, port, data) {
    window.pos.printer.send({
      host,
      port,
      data
    });
  }

  function validateHostValue(v) {
    return /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/.test(v);
  }

  function validatePortValue(v) {
    return /^[0-9]+$/g.test(v);
  }

  function validateMsgValue(v) {
    return typeof v === 'string' && v.length > 0;
  }
})();
