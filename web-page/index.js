/* eslint-disable no-var, no-console */

var path = require('path');
var express = require('express');
var os = require('os');

var cfg = {
  staticFilesDir: path.resolve(__dirname, 'static'),
  host: os.platform() === 'linux' ? '0.0.0.0' : '127.0.0.1',
  port: 3000
};

var app = express();

app.use(express.static(cfg.staticFilesDir));

app.listen(cfg.port, cfg.host, () => {
  console.log(`static server on http://${cfg.host}:${cfg.port}`);
});
